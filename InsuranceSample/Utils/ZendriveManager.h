//
//  ZendriveManager.h
//  InsuranceSample
//
//  Created by Jishnu Nair on 28/11/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InsurancePeriod : NSObject

@property (nonatomic, readonly) int period;
@property (nonatomic, readonly) NSString *trackingId;
@end

@interface ZendriveManager : NSObject

+ (instancetype)sharedInstance;
- (void)initializeSDKForDriverId:(NSString *)driverId
                  successHandler:(void (^)(void))successBlock
               andFailureHandler:(void (^)(NSError *))failureBlock;
- (void)updateInsurancePeriodsBasedOnApplicationState;
- (InsurancePeriod *)currentlyActiveInsurancePeriod;
@end
