//
//  LocationPermissionManager.m
//  InsuranceSample
//
//  Created by Yogesh on 11/18/16.
//  Copyright © 2016 Zendrive. All rights reserved.
//

#import "LocationPermissionManager.h"
#import <CoreLocation/CoreLocation.h>

#import "AppDelegate.h"

__strong static LocationPermissionManager *_sharedInstance = nil;

@interface LocationPermissionManager()<CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) UIAlertController *locationPermissionAlert;
@property (nonatomic) UIWindow *alertWindow;
@end

@implementation LocationPermissionManager

+ (instancetype)sharedInstance {
    @synchronized(self) {
        return _sharedInstance;
    }
}

+ (void)setup {
    @synchronized(self) {
        if (_sharedInstance == nil) {
            _sharedInstance = [[self alloc] init];
        }
    }
}

+ (void)teardown {
    @synchronized (self) {
        [_sharedInstance hideLocationPermissionErrorViewIfVisible];
        _sharedInstance = nil;
    }
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    return self;
}

//------------------------------------------------------------------------------
#pragma mark - CLLocationManagerDelegate
//------------------------------------------------------------------------------
- (void)locationManager:(CLLocationManager *)manager
didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            // Display location permisison view controller
            [self displayLocationPermissionErrorViewIfNotVisible];
            // Follow through to ask for permission
        case kCLAuthorizationStatusNotDetermined:
            // Request for location, specifically for iOS8
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_locationManager requestAlwaysAuthorization];
            }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
            // Remove location permission view controller
            [self hideLocationPermissionErrorViewIfVisible];
            break;
    }
}

//------------------------------------------------------------------------------
#pragma mark - ViewController display handling
//------------------------------------------------------------------------------
- (void)displayLocationPermissionErrorViewIfNotVisible {
    if (!_locationPermissionAlert) {
        NSString *errorMessage = @"Please provide \"Always Allow\" location"
        " permission to get insurance benefits";
        _locationPermissionAlert =
        [UIAlertController alertControllerWithTitle:@"Location Permission Denied"
                                            message:errorMessage
                                     preferredStyle:UIAlertControllerStyleAlert];
        __weak LocationPermissionManager *weakself = self;
        [_locationPermissionAlert addAction:[UIAlertAction actionWithTitle:@"Open Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication]
             openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            LocationPermissionManager *strongself = weakself;
            if (strongself) {
                [strongself showAlert:strongself.locationPermissionAlert];
            }
        }]];

        [self showAlert:_locationPermissionAlert];
    }
}

- (void)showAlert:(UIAlertController *)alert {
    if (!_alertWindow) {
        _alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _alertWindow.rootViewController = [UIViewController new];
        _alertWindow.windowLevel = UIWindowLevelAlert + 1;
        [_alertWindow makeKeyAndVisible];
    }
    [_alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
    _locationPermissionAlert = alert;
}

- (void)hideLocationPermissionErrorViewIfVisible {
    if (_locationPermissionAlert == nil) {
        return;
    }
    __weak LocationPermissionManager *weakself = self;
    [_locationPermissionAlert dismissViewControllerAnimated:YES completion:^{
        LocationPermissionManager *strongself = weakself;
        if (strongself) {
            strongself.alertWindow = nil;
        }
    }];
    _locationPermissionAlert = nil;
}
@end
