//
//  UserDefaultsManager.h
//  InsuranceSample
//
//  Created by Yogesh on 11/22/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SharedUserDefaultsManager [UserDefaultsManager sharedInstance]

@interface UserDefaultsManager : NSObject

+ (instancetype)sharedInstance;

//------------------------------------------------------------------------------
#pragma mark - User Data Management
//------------------------------------------------------------------------------
- (void)setDriverId:(NSString *)driverId;
- (NSString *)driverId;

//------------------------------------------------------------------------------
#pragma mark - TripManager persistent data
//------------------------------------------------------------------------------
- (void)setPassengersInCar:(int)passengersInCar;
- (int)passengersInCar;

- (void)setPassengersWaitingForPickup:(int)passengersWaitingForPickup;
- (int)passengersWaitingForPickup;

- (void)setTrackingId:(NSString *)trackingId;
- (NSString *)trackingId;

- (void)setIsUserOnDuty:(BOOL)isUserOnduty;
- (BOOL)isUserOnDuty;
@end
