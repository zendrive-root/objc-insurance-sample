//
//  LocationPermissionManager.h
//  InsuranceSample
//
//  Created by Yogesh on 11/18/16.
//  Copyright © 2016 Zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationPermissionManager : NSObject

+ (void)setup;
+ (void)teardown;
@end
