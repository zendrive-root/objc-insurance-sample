//
//  AppLogger1.h
//  InsuranceSample
//
//  Created by Yogesh on 4/23/15.
//  Copyright (c) 2015 Zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLog.h>
static int ddLogLevel = DDLogLevelVerbose;

@interface AppLogger : NSObject

+ (void)initializeDefaultLoggers;
+ (void)initializeLogglyLoggerForDriverId:(NSString *)userId;
@end
