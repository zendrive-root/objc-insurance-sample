//
//  AppLogger1.m
//  InsuranceSample
//
//  Created by Yogesh on 4/23/15.
//  Copyright (c) 2015 Zendrive. All rights reserved.
//

#import "AppLogger.h"
#import <CocoaLumberjack/DDFileLogger.h>
#import <CocoaLumberjack/DDTTYLogger.h>

#import "LogglyLogger.h"
#import "LogglyFormatter.h"

static LogglyLogger *logglyLogger;
@implementation AppLogger

+ (void)initializeDefaultLoggers {
    // File logger -- Verbose
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 60 * 60 * 24;  // 24 hours
    fileLogger.logFileManager.maximumNumberOfLogFiles = 20;
    [DDLog addLogger:fileLogger withLevel:DDLogLevelVerbose];

#ifdef DEBUG
    // Duplicate to NSLog, enable only in debug
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:DDLogLevelVerbose];
#endif

    DDLogInfo(@"Saving logs to directory %@", fileLogger.logFileManager.logsDirectory);
}

+ (void)initializeLogglyLoggerForDriverId:(NSString *)userId {
    // Loggly logger -- Debug
    NSString *logglyKey = nil; // Replace with your loggly key
    if (!logglyKey) {
        DDLogWarn(@"Loggly key not available");
        return;
    }

    if (logglyLogger) {
        [DDLog removeLogger:logglyLogger];
        logglyLogger = nil;
    }

    NSString *tags = [NSString stringWithFormat:@"%@,%@", @"ios", userId];

    logglyLogger = [[LogglyLogger alloc] init];
    [logglyLogger setLogFormatter:[[LogglyFormatter alloc] init]];
    logglyLogger.logglyKey = logglyKey; // Replace your loggly key

    logglyLogger.logglyTags = tags;

    logglyLogger.saveInterval = 10; // Modify as required, small number means more network calls
    // and thereby more battery drain but bigger number means there's a higher chance of losing
    // the logs.
    [DDLog addLogger:logglyLogger withLevel:DDLogLevelDebug];
}

@end
