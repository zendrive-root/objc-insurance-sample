
#import "ZendriveManager.h"
#import <ZendriveSDK/Zendrive.h>
#import <ZendriveSDK/ZendriveInsurance.h>
#import "TripManager.h"
#import "AppLogger.h"

static NSString * kZendriveSDKKeyString = @"your-zendrive-sdk-key";
static ZendriveManager *_sharedInstance;

@implementation InsurancePeriod

- (instancetype)initWithPeriod:(int)period {
    return [self initWithPeriod:period trackingId:nil];
}

- (instancetype)initWithPeriod:(int)period trackingId:(NSString *)trackingId {
    self = [super init];
    if (self) {
        _period = period;
        _trackingId = trackingId;
    }
    return self;
}
@end

@interface ZendriveManager()<ZendriveDelegateProtocol>

@end

@implementation ZendriveManager

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if (_sharedInstance == nil) {
            _sharedInstance = [[self alloc] init];
        }
        return _sharedInstance;
    }
}

- (InsurancePeriod *)currentlyActiveInsurancePeriod {
    TMState *appState = [[TripManager sharedInstance] tripManagerState];
    if (!appState.isUserOnDuty) {
        return nil;
    }
    else if (appState.passengersInCar > 0) {
        return [[InsurancePeriod alloc] initWithPeriod:3 trackingId:appState.trackingId];
    }
    else if (appState.passengersWaitingForPickup > 0) {
        return [[InsurancePeriod alloc] initWithPeriod:2 trackingId:appState.trackingId];
    }
    else {
        return [[InsurancePeriod alloc] initWithPeriod:1];
    }
}

//------------------------------------------------------------------------------
#pragma mark - Error parsing
//------------------------------------------------------------------------------
- (NSError *)getDisplayableErrorUsingZendriveError:(NSError *)zendriveError {
    NSString *message = @"Unknown error in setting up for insurance, please restart the application."
    " Please contact support if the issue persists";
    if (zendriveError.code == kZendriveErrorNetworkUnreachable) {
        message = @"Internet not available to set up for insurance, please enable 3G/LTE or connect"
        " to wifi and restart the application";
    }
    NSError *error = [[NSError alloc]
                      initWithDomain:@"ZendriveManager"
                      code:zendriveError.code
                      userInfo:@{NSLocalizedFailureReasonErrorKey: message}];
    return error;
}

//------------------------------------------------------------------------------
#pragma mark - Zendrive API calls
//------------------------------------------------------------------------------
- (void)initializeSDKForDriverId:(NSString *)driverId
                  successHandler:(void (^)(void))successBlock
               andFailureHandler:(void (^)(NSError *))failureBlock {
    [self initializeSDKForDriverId:driverId successHandler:successBlock
                 andFailureHandler:failureBlock trialNumber:1 totalRetryCount:3];
}

- (void)initializeSDKForDriverId:(NSString *)driverId
                  successHandler:(void (^)(void))successBlock
               andFailureHandler:(void (^)(NSError *))failureBlock
                     trialNumber:(int)trialNumber
                 totalRetryCount:(int)totalRetryCount {
    InsurancePeriod *activeInsurancePeriod = [self currentlyActiveInsurancePeriod]; //This assumes activePeriod nil as no-tracking period
    DDLogInfo(@"[ZendriveManager]: initializing ZendriveSDK");

    ZendriveConfiguration *configuration = [[ZendriveConfiguration alloc] init];
    configuration.applicationKey = kZendriveSDKKeyString;
    configuration.driveDetectionMode =
    (activeInsurancePeriod) ? ZendriveDriveDetectionModeAutoON:ZendriveDriveDetectionModeAutoOFF;

    configuration.driverId = driverId;

    [Zendrive
     setupWithConfiguration:configuration delegate:self
     completionHandler: ^(BOOL success, NSError *error) {
         if(error) {
             if (trialNumber < totalRetryCount) {
                 [self initializeSDKForDriverId:driverId successHandler:successBlock
                              andFailureHandler:failureBlock trialNumber:(trialNumber + 1)
                                totalRetryCount:totalRetryCount];
                 return;
             }
             DDLogError(@"[ZendriveManager]: setupWithConfiguration:error: %@",
                        error.localizedFailureReason);

             error = [self getDisplayableErrorUsingZendriveError:error];
             if (failureBlock) {
                 failureBlock(error);
             }
         } else {
             DDLogInfo(@"[ZendriveManager]: setupWithConfiguration:success");
             InsurancePeriod *activeInsurancePeriod = [self currentlyActiveInsurancePeriod];
             if (activeInsurancePeriod) {
                 [Zendrive setDriveDetectionMode:ZendriveDriveDetectionModeAutoON];
             }
             [self updateInsurancePeriodsBasedOnApplicationState];
             if (successBlock) {
                 successBlock();
             }
         }
     }];
}

- (void)updateInsurancePeriodsBasedOnApplicationState {
    InsurancePeriod *activeInsurancePeriod = [self currentlyActiveInsurancePeriod]; // This assumes activePeriod nil as no-tracking period
    NSError *error = nil;
    if (!activeInsurancePeriod) {
        DDLogInfo(@"[ZendriveManager]: updateInsurancePeriodsBasedOnApplicationState"
                  " with NO Period");
        [ZendriveInsurance stopPeriod:&error];
    }
    else {
        switch (activeInsurancePeriod.period) {
            case 1:
                DDLogInfo(@"[ZendriveManager]: updateInsurancePeriodsBasedOnApplicationState"
                          " with Period: %i and trackingId: %@", activeInsurancePeriod.period,
                          activeInsurancePeriod.trackingId);
                [ZendriveInsurance startPeriod1:&error];
                break;
            case 2:
                DDLogInfo(@"[ZendriveManager]: updateInsurancePeriodsBasedOnApplicationState"
                          " with Period: %i and trackingId: %@", activeInsurancePeriod.period,
                          activeInsurancePeriod.trackingId);
                [ZendriveInsurance startDriveWithPeriod2:activeInsurancePeriod.trackingId error:&error];
                break;
            case 3:
                DDLogInfo(@"[ZendriveManager]: updateInsurancePeriodsBasedOnApplicationState"
                          " with Period: %i and trackingId: %@", activeInsurancePeriod.period,
                          activeInsurancePeriod.trackingId);
                [ZendriveInsurance startDriveWithPeriod3:activeInsurancePeriod.trackingId error:&error];
                break;

            default:
                DDLogError(@"[ZendriveManager]: updateInsurancePeriodsBasedOnApplicationState"
                           " with WRONG Period");
                [ZendriveInsurance stopPeriod:&error];
                break;
        }
    }
    if (error && error.code != kZendriveErrorInsurancePeriodSame) {
        // Something went wrong, log the error
        DDLogError(@"[ZendriveManager]: Error in period switch: %li", (long)error.code);
    }
}

//------------------------------------------------------------------------------
#pragma mark - ZendriveDelegateProtocol
//------------------------------------------------------------------------------
- (void)processStartOfDrive:(ZendriveDriveStartInfo *)startInfo {
    DDLogInfo(@"[ZendriveManager]: Start of Drive invoked");
}

- (void)processResumeOfDrive:(ZendriveDriveResumeInfo *)drive {
    DDLogInfo(@"[ZendriveManager]: Resume of Drive invoked");
}

- (void)processEndOfDrive:(ZendriveEstimatedDriveInfo *)drive {
    DDLogInfo(@"[ZendriveManager]: End of Drive invoked");
}

- (void)processAnalysisOfDrive:(ZendriveAnalyzedDriveInfo *)drive {
    DDLogInfo(@"[ZendriveManager]: Analysis of Drive invoked");
}

- (void)processLocationDenied {
    DDLogInfo(@"[ZendriveManager]: User denied Location to Zendrive SDK.");
}

- (void)processLocationApproved   {
    DDLogInfo(@"[ZendriveManager]: User approved Location to Zendrive SDK.");
}

- (void)processAccidentDetected:(ZendriveAccidentInfo *)accidentInfo {
    DDLogInfo(@"[ZendriveManager]: Accident detected by Zendrive SDK.");
}

@end
