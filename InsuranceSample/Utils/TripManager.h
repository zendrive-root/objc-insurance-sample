//
//  TripManager.h
//  InsuranceSample
//
//  Created by Yogesh on 11/22/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import <Foundation/Foundation.h>

//------------------------------------------------------------------------------
#pragma mark - TripManager State
//------------------------------------------------------------------------------
@interface TMState : NSObject

@property (nonatomic, readonly) BOOL isUserOnDuty;
@property (nonatomic, readonly) int passengersInCar;
@property (nonatomic, readonly) int passengersWaitingForPickup;
@property (nonatomic, readonly) NSString *trackingId;
@end

//------------------------------------------------------------------------------
#pragma mark - TripManager
//------------------------------------------------------------------------------
@interface TripManager : NSObject

+ (instancetype)sharedInstance;

- (void)goOnDuty;
- (void)goOffDuty;
- (void)acceptNewPassengerRequest;
- (void)cancelPassengerRequest;
- (void)pickAPassenger;
- (void)dropAPassenger;

- (TMState *)tripManagerState;
@end
