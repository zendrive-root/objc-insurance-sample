//
//  TripManager.m
//  InsuranceSample
//
//  Created by Yogesh on 11/22/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "TripManager.h"
#import "UserDefaultsManager.h"
#import "ZendriveManager.h"

#import "LocationPermissionManager.h"

//------------------------------------------------------------------------------
#pragma mark - TripManager State
//------------------------------------------------------------------------------
@interface TMState()

@property (nonatomic) BOOL isUserOnDuty;
@property (nonatomic) int passengersInCar;
@property (nonatomic) int passengersWaitingForPickup;
@property (nonatomic) NSString *trackingId;
@end

@implementation TMState

- (instancetype)initWithIsUserOnDuty:(BOOL)isUserOnDuty passengersInCar:(int)passengersInCar
          passengersWaitingForPickup:(int)passengersWaitingForPickup
                          trackingId:(NSString *)trackingId {
    self = [super init];
    if (self) {
        _isUserOnDuty = isUserOnDuty;
        _passengersInCar = passengersInCar;
        _passengersWaitingForPickup = passengersWaitingForPickup;
        _trackingId = trackingId;
    }
    return self;
}

- (id)copy {
    return [[TMState alloc] initWithIsUserOnDuty:_isUserOnDuty
                                 passengersInCar:_passengersInCar
                      passengersWaitingForPickup:_passengersWaitingForPickup
                                      trackingId:_trackingId];
}
@end

//------------------------------------------------------------------------------
#pragma mark - TripManager Implementation
//------------------------------------------------------------------------------
static TripManager *_sharedInstance;
@interface TripManager()

@property (nonatomic) TMState *state;
@end

@implementation TripManager

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[self alloc] init];;
        }
        return _sharedInstance;
    }
}

- (instancetype)init {
    self = [super init];
    if (self) {
        UserDefaultsManager *userDefaultsManager = SharedUserDefaultsManager;
        _state = [[TMState alloc]
                  initWithIsUserOnDuty:userDefaultsManager.isUserOnDuty
                  passengersInCar:userDefaultsManager.passengersInCar
                  passengersWaitingForPickup:userDefaultsManager.passengersWaitingForPickup
                  trackingId:userDefaultsManager.trackingId];
        [self setupOrTeardownLocationPermissionManager];
    }
    return self;
}

- (void)goOnDuty {
    @synchronized(self) {
        _state.isUserOnDuty = YES;
        [SharedUserDefaultsManager setIsUserOnDuty:_state.isUserOnDuty];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
        [self setupOrTeardownLocationPermissionManager];
    }
}

- (void)goOffDuty {
    @synchronized(self) {
        _state.isUserOnDuty = NO;
        [SharedUserDefaultsManager setIsUserOnDuty:_state.isUserOnDuty];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
        [self setupOrTeardownLocationPermissionManager];
    }
}

- (void)acceptNewPassengerRequest {
    @synchronized(self) {
        _state.passengersWaitingForPickup += 1;
        [SharedUserDefaultsManager setPassengersWaitingForPickup:_state.passengersWaitingForPickup];
        [self updateTrackingIdIfNeeded];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
    }
}

- (void)cancelPassengerRequest {
    @synchronized(self) {
        _state.passengersWaitingForPickup -= 1;
        [SharedUserDefaultsManager setPassengersWaitingForPickup:_state.passengersWaitingForPickup];
        [self updateTrackingIdIfNeeded];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
    }
}

- (void)pickAPassenger {
    @synchronized(self) {
        _state.passengersInCar += 1;
        [SharedUserDefaultsManager setPassengersInCar:_state.passengersInCar];
        _state.passengersWaitingForPickup -= 1;
        [SharedUserDefaultsManager setPassengersWaitingForPickup:_state.passengersWaitingForPickup];
        [self updateTrackingIdIfNeeded];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
    }
}

- (void)dropAPassenger {
    @synchronized(self) {
        _state.passengersInCar -= 1;
        [SharedUserDefaultsManager setPassengersInCar:_state.passengersInCar];
        [self updateTrackingIdIfNeeded];
        [[ZendriveManager sharedInstance] updateInsurancePeriodsBasedOnApplicationState];
    }
}

- (void)updateTrackingIdIfNeeded {
    if (_state.passengersInCar > 0 || _state.passengersWaitingForPickup > 0) {
        if (!_state.trackingId) {
            // Generate random id for this trip, using current epoch timestamp
            _state.trackingId = [NSString stringWithFormat:@"%.0f",
                               [[NSDate date] timeIntervalSince1970]*1000];
            [SharedUserDefaultsManager setTrackingId:_state.trackingId];
        }
    }
    else if (_state.trackingId) {
        _state.trackingId = nil;
        [SharedUserDefaultsManager setTrackingId:_state.trackingId];
    }
}

- (int)passengersInCar {
    @synchronized(self) {
        return _state.passengersInCar;
    }
}

- (int)passengersWaitingForPickup {
    @synchronized(self) {
        return _state.passengersWaitingForPickup;
    }
}

- (TMState *)tripManagerState {
    @synchronized(self) {
        return [[TMState alloc] initWithIsUserOnDuty:_state.isUserOnDuty
                                     passengersInCar:_state.passengersInCar
                          passengersWaitingForPickup:_state.passengersWaitingForPickup
                                          trackingId:_state.trackingId];
    }
}

- (void)setupOrTeardownLocationPermissionManager {
    if (_state.isUserOnDuty) {
        [LocationPermissionManager setup];
    }
    else {
        [LocationPermissionManager teardown];
    }
}

@end
