//
//  UserDefaultsManager.m
//  InsuranceSample
//
//  Created by Yogesh on 11/22/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "UserDefaultsManager.h"

static NSString *kDriverIdKey = @"driverId";

static NSString *kIsUserOnDuty = @"isUserOnDuty";
static NSString *kPassengersInCarKey = @"passengersInCar";
static NSString *kPassengersWaitingForPickup = @"passengersWaitingForPickup";
static NSString *kTrackingId = @"trackingId";

static UserDefaultsManager *_sharedInstance;

@interface UserDefaultsManager()

@property (nonatomic) NSUserDefaults *userDefaults;
@end

@implementation UserDefaultsManager

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if (!_sharedInstance) {
            _sharedInstance = [[self alloc] init];;
        }
        return _sharedInstance;
    }
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

//------------------------------------------------------------------------------
#pragma mark - User Data Management
//------------------------------------------------------------------------------
- (void)setDriverId:(NSString *)driverId {
    [_userDefaults setObject:driverId forKey:kDriverIdKey];
    [_userDefaults synchronize];
}

- (NSString *)driverId {
    return [_userDefaults objectForKey:kDriverIdKey];
}

//------------------------------------------------------------------------------
#pragma mark - TripManager
//------------------------------------------------------------------------------
- (void)setIsUserOnDuty:(BOOL)isUserOnduty {
    [_userDefaults setObject:@(isUserOnduty) forKey:kIsUserOnDuty];
    [_userDefaults synchronize];
}

- (BOOL)isUserOnDuty {
    return ((NSNumber *)[_userDefaults objectForKey:kIsUserOnDuty]).boolValue;
}

- (void)setPassengersInCar:(int)passengersInCar {
    [_userDefaults setObject:@(passengersInCar) forKey:kPassengersInCarKey];
    [_userDefaults synchronize];
}

- (int)passengersInCar {
    return ((NSNumber *)[_userDefaults objectForKey:kPassengersInCarKey]).intValue;
}

- (void)setPassengersWaitingForPickup:(int)passengersWaitingForPickup {
    [_userDefaults setObject:@(passengersWaitingForPickup) forKey:kPassengersWaitingForPickup];
    [_userDefaults synchronize];
}

- (int)passengersWaitingForPickup {
    return ((NSNumber *)[_userDefaults objectForKey:kPassengersWaitingForPickup]).intValue;
}

- (void)setTrackingId:(NSString *)trackingId {
    [_userDefaults setObject:trackingId forKey:kTrackingId];
    [_userDefaults synchronize];
}

- (NSString *)trackingId {
    return [_userDefaults objectForKey:kTrackingId];
}

@end
