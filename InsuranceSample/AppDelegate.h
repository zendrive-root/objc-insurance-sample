//
//  AppDelegate.h
//  InsuranceSample
//
//  Created by Yogesh on 11/21/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *rootNavigationController;

- (void)reloadApplication;
@end
