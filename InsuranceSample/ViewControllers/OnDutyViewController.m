//
//  OnDutyViewController.m
//  InsuranceSample
//
//  Created by Yogesh on 11/27/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "OnDutyViewController.h"
#import "TripManager.h"
#import "OffDutyViewController.h"
#import "ZendriveManager.h"
#import "AppLogger.h"

@interface OnDutyViewController ()

@property (nonatomic) IBOutlet UIButton *acceptARequestButton;
@property (nonatomic) IBOutlet UIButton *cancelPickupButton;
@property (nonatomic) IBOutlet UIButton *pickAPassengerButton;
@property (nonatomic) IBOutlet UIButton *dropPassengerButton;
@property (nonatomic) IBOutlet UIButton *goOffDutyButton;

@property (nonatomic) IBOutlet UILabel *insurancePeriodLabel;
@property (nonatomic) IBOutlet UILabel *passengerInCarLabel;
@property (nonatomic) IBOutlet UILabel *passengerWaitingForPickupLabel;
@end

@implementation OnDutyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"On Duty";
    [self reloadUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)acceptNewRequestButtonTapped:(id)sender {
    DDLogInfo(@"[OnDutyViewController]: acceptNewRequestButtonTapped");
    [[TripManager sharedInstance] acceptNewPassengerRequest];
    [self reloadUI];
}

- (IBAction)cancelPickupButtonTapped:(id)sender {
    DDLogInfo(@"[OnDutyViewController]: cancelPickupButtonTapped");
    [[TripManager sharedInstance] cancelPassengerRequest];
    [self reloadUI];
}

- (IBAction)pickupPassengerButtonTapped:(id)sender {
    DDLogInfo(@"[OnDutyViewController]: pickupPassengerButtonTapped");
    [[TripManager sharedInstance] pickAPassenger];
    [self reloadUI];
}

- (IBAction)dropPassengerButtonTapped:(id)sender {
    DDLogInfo(@"[OnDutyViewController]: dropPassengerButtonTapped");
    [[TripManager sharedInstance] dropAPassenger];
    [self reloadUI];
}

- (IBAction)goOffDutyButtonTapped:(id)sender {
    DDLogInfo(@"[OnDutyViewController]: goOffDutyButtonTapped");
    [[TripManager sharedInstance] goOffDuty];
    [self.navigationController setViewControllers:
     @[[[OffDutyViewController alloc] init]] animated:YES];
}

- (void)reloadUI {
    TMState *tripManagerState = [[TripManager sharedInstance] tripManagerState];
    InsurancePeriod *insurancePeriod =
    [[ZendriveManager sharedInstance] currentlyActiveInsurancePeriod];

    // Update text
    self.insurancePeriodLabel.text = [NSString stringWithFormat:@"Insurance Period: %i",
                                      insurancePeriod.period];

    self.passengerInCarLabel.text = [NSString stringWithFormat:@"Passengers In Car: %i",
                                     tripManagerState.passengersInCar];

    self.passengerWaitingForPickupLabel.text =
    [NSString stringWithFormat:@"Passengers awaiting pickup: %i",
     tripManagerState.passengersWaitingForPickup];

    // Enable/Disable buttons
    self.dropPassengerButton.enabled = (tripManagerState.passengersInCar > 0);
    self.pickAPassengerButton.enabled = (tripManagerState.passengersWaitingForPickup > 0);
    self.cancelPickupButton.enabled = (tripManagerState.passengersWaitingForPickup > 0);
    self.goOffDutyButton.enabled = (tripManagerState.passengersInCar == 0 &&
                                    tripManagerState.passengersWaitingForPickup == 0);
}

@end
