//
//  LoginViewController.m
//  InsuranceSample
//
//  Created by Yogesh on 11/22/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "LoginViewController.h"
#import "UserDefaultsManager.h"

#import "AppDelegate.h"

@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *driverIdField;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.title = @"Login";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpButtonTapped:(id)sender {
    NSString *driverId = _driverIdField.text;
    if (driverId.length > 0) {
        [SharedUserDefaultsManager setDriverId:driverId];
        [((AppDelegate *)[UIApplication sharedApplication].delegate) reloadApplication];
    }
}

@end
