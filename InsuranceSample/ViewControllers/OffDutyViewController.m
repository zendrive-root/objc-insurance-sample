//
//  OffDutyViewController.m
//  InsuranceSample
//
//  Created by Yogesh on 11/27/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "OffDutyViewController.h"
#import "TripManager.h"
#import "OnDutyViewController.h"
#import "AppLogger.h"

@interface OffDutyViewController ()

@end

@implementation OffDutyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Off Duty";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ondutyButtonTapped:(id)sender {
    DDLogInfo(@"[OffDutyViewController]: ondutyButtonTapped");
    [[TripManager sharedInstance] goOnDuty];
    [self.navigationController setViewControllers:@[[[OnDutyViewController alloc] init]] animated:YES];
}

@end
