//
//  main.m
//  InsuranceSample
//
//  Created by Yogesh on 11/21/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
