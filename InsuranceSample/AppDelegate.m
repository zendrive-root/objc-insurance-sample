//
//  AppDelegate.m
//  InsuranceSample
//
//  Created by Yogesh on 11/21/17.
//  Copyright © 2017 Zendrive. All rights reserved.
//

#import "AppDelegate.h"
#import "AppLogger.h"
#import "UserDefaultsManager.h"
#import "TripManager.h"

#import "LoginViewController.h"
#import "OffDutyViewController.h"
#import "OnDutyViewController.h"
#import "ZendriveManager.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _window.backgroundColor = [UIColor whiteColor];
    [_window makeKeyAndVisible];

    [self initializeApplication:application options:launchOptions];
    DDLogInfo(@"[AppDelegate]: didFinishLaunchingWithOptions");
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    DDLogInfo(@"[AppDelegate]: applicationWillResignActive");
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    DDLogInfo(@"[AppDelegate]: applicationDidEnterBackground");
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    DDLogInfo(@"[AppDelegate]: applicationWillEnterForeground");
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    DDLogInfo(@"[AppDelegate]: applicationDidBecomeActive");
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    DDLogInfo(@"[AppDelegate]: applicationWillTerminate");
}

//------------------------------------------------------------------------------
#pragma mark - Application Initialization
//------------------------------------------------------------------------------
- (void)initializeApplication:(UIApplication *)application options:(NSDictionary *)launchOptions {
    // Start crucial components like logger, crash reporting etc.
    [AppLogger initializeDefaultLoggers];

    // Load the application
    [self reloadApplication];
}

- (void)reloadApplication {
    NSString *driverId = [SharedUserDefaultsManager driverId];
    DDLogInfo(@"[AppDelegate]: reloadApplication with driverId:%@", driverId);
    if (driverId) {
        if (!_window.rootViewController) {
            // It is generally better to display a loading page while singletons and important
            // libraries are loaded before loading the initial view controller, this will ensure
            // loading complex UI in memory does not affect the core functionality of the
            // application
            UIViewController *launchViewController =
            [[UIStoryboard storyboardWithName:@"LaunchScreen" bundle:[NSBundle mainBundle]]
             instantiateInitialViewController];
            _window.rootViewController = launchViewController;
        }

        [MBProgressHUD showHUDAddedTo:_window animated:YES];

        // Load any singletons or libraries that need user information, e.g. logger, ZendriveSDK
        [AppLogger initializeLogglyLoggerForDriverId:driverId];
        [[ZendriveManager sharedInstance] initializeSDKForDriverId:driverId successHandler:^{
            [MBProgressHUD hideHUDForView:_window animated:YES];

            // All important non-UI components loaded, load application UI
            [self loadInitialViewControllerWithDriverId:driverId];
        } andFailureHandler:^(NSError *error) {
            [MBProgressHUD hideHUDForView:_window animated:YES];
            UIAlertController *alert =
            [UIAlertController alertControllerWithTitle:@"Initialization Failed"
                                                message:error.localizedFailureReason
                                         preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self reloadApplication];
            }]];
            [_window.rootViewController presentViewController:alert animated:YES completion:nil];
        }];
    }
    else {
        // Disable everything that uses driverId, e.g. ZendriveSDK
        [[TripManager sharedInstance] goOffDuty];

        // Load UI
        [self loadInitialViewControllerWithDriverId:driverId];
    }
}

//------------------------------------------------------------------------------
#pragma mark application loading methods
//------------------------------------------------------------------------------
- (void)loadInitialViewControllerWithDriverId:(NSString *)driverId {
    DDLogInfo(@"[AppDelegate]: loadInitialViewController");
    UIViewController *initialViewController;

    if (driverId) {
        // User info available, open either OffDuty or OnDuty View Controller based on the state
        if ([[UserDefaultsManager sharedInstance] isUserOnDuty]) {
            initialViewController = [[OnDutyViewController alloc] init];
        }
        else {
            initialViewController = [[OffDutyViewController alloc] init];
        }
    }
    else {
        // No user information available, open LoginViewController
        initialViewController = [[LoginViewController alloc] init];
    }

    if (!_rootNavigationController) {
        _rootNavigationController =
        [[UINavigationController alloc] initWithRootViewController:initialViewController];
        _rootNavigationController.navigationBar.barTintColor =
        [UIColor colorWithRed:56/256.0 green:159/256.0 blue:116/256.0 alpha:1.0];
        _rootNavigationController.navigationBar.titleTextAttributes =
        @{NSForegroundColorAttributeName:[UIColor whiteColor]};
        _window.rootViewController = _rootNavigationController;
    }
    else {
        [_rootNavigationController setViewControllers:@[initialViewController] animated:YES];
    }
}

@end
